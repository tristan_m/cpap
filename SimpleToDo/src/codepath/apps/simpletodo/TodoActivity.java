package codepath.apps.simpletodo;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

public class TodoActivity extends Activity {
	
	private final int EDITITEM_REQUEST_CODE = 10; // Request code for editing activity
	ArrayList<String> items;                      // Items list
	ArrayAdapter<String> itemsAdapter;
	int currentListPos;                           // Hit item index in the list

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_todo);
		ListView lvItems = (ListView)findViewById(R.id.lvItems);  // To-do items list
		items = new ArrayList<String>();
		readItems();                              // Read to-do items from saved file
		itemsAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,items);
		lvItems.setAdapter(itemsAdapter);
		itemsAdapter.notifyDataSetInvalidated();
		// items.add("First item");
		// items.add("Second item");
		setupListViewListener(lvItems);          // Listen to click and long-click event
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.todo, menu);
		return true;
	}
	
	/* When the Add button is clicked, add an item to the to-do list */
	public void addTodoItem(View v) {
		EditText etNewItem = (EditText)findViewById(R.id.etNewItem);
		String newItem = etNewItem.getText().toString();
		if (!newItem.isEmpty()) {    // Let's not add anything if the input is empty
			items.add(etNewItem.getText().toString());
		    // itemsAdapter.add(etNewItem.getText().toString());
			itemsAdapter.notifyDataSetInvalidated();
		    etNewItem.setText("");   // Clear up the input text field
		    saveItems();             // Save changes to file
		}
	}
	
	/* Set up two listeners on the item list view: click and long-click */
	private void setupListViewListener(ListView lvItems) {
		// Listen to the long-click event: remove item
		lvItems.setOnItemLongClickListener(new OnItemLongClickListener() {
			public boolean onItemLongClick(AdapterView<?> aView, View item, int pos, long id) {
				items.remove(pos);
				itemsAdapter.notifyDataSetInvalidated();
				saveItems();         // Save changes to file
				return true;
			}
		});
		// Listen to the click event: edit item
		lvItems.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> aView, View item, int pos, long id) {
				String editingItem = items.get(pos);   // Get the hit item
				currentListPos = pos;                  // Record the hit item index
				launchComposeView(editingItem);        // Open the edit-item activity
			}
		});
	}
    
	// Read items from a saved file
	private void readItems() {
		File filesDir = getFilesDir();   // Get the default data file directory /data/data/codepath.apps.simpletodo/files
		File file = new File(filesDir, "todo.txt");
		// If the file doesn't exist, let's not bother to read
		if (!file.exists()) {
			return;
		}
		try {
			InputStream is = openFileInput("todo.txt");
			if (is != null) {
				// Read the saved file line by line to the item list
				InputStreamReader isr = new InputStreamReader(is);
				BufferedReader br = new BufferedReader (isr);
			    String line="";
			    while ((line = br.readLine())!=null) {
			    	items.add(line);
			    }
			}
		}
    	catch (IOException e) {
			e.printStackTrace();
		}
	}
 
	// Write item list to the saved file
    private void saveItems() {
		try {
			OutputStreamWriter osw = new OutputStreamWriter(openFileOutput("todo.txt", Context.MODE_PRIVATE));
			for (String item: items) {
				osw.write(item + "\n");
			}
			osw.close();
		}
    	catch (IOException e) {
			e.printStackTrace();
		}
    }
    
    /* Open the editing activity window */
    private void launchComposeView(String item) {
    	Intent i = new Intent(TodoActivity.this, EditItemActivity.class);
    	i.putExtra("editingItem", item);    // Pass the item text
    	startActivityForResult(i, EDITITEM_REQUEST_CODE);
    }
    
    /* Get back the edited item */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent i) {
    	if (requestCode==EDITITEM_REQUEST_CODE && resultCode==RESULT_OK) {
    		// Retrieve the passed value - the edited item text
    		String editingItem = i.getExtras().getString("editingItem");
    		if (editingItem.equals("")) {
    			// If the returning edited item is empty, that means user wants to delete this item
    			items.remove(currentListPos);
    		}
    		else {
    			// currentListPos has been recorded before editing activity starts
    			items.set(currentListPos, editingItem);
    		}
			itemsAdapter.notifyDataSetInvalidated();
			saveItems();             // Save changes to the file
    	}
    }
}
    