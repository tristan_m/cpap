package codepath.apps.simpletodo;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;

public class EditItemActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_edit_item);
		// Passed data from last activity view - editing item
		String editingItem = getIntent().getStringExtra("editingItem");
		EditText etEditItem = (EditText)findViewById(R.id.etEditItem);
		etEditItem.setText(editingItem);    // Pre-populate the editing field
		etEditItem.requestFocus();          // Set focus
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.edit_item, menu);
		return true;
	}
	
	
	/* When the Save button is clicked, send back the edited item text to the todo-activity window, 
	 * then close this edit tiem activity itself  
	 */
	public void submitEdited(View v) {
		EditText etEditItem = (EditText)findViewById(R.id.etEditItem);
		// Use Intent to pass back the edited item text
		Intent i = new Intent();
		i.putExtra("editingItem", etEditItem.getText().toString());
		setResult(RESULT_OK, i);
		// closes the activity and returns to first screen
		this.finish();
	}

}
