package com.codepath.apps.tipscalculator;

import java.text.DecimalFormat;

import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {
    int tipsRate = 0;
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
        EditText etAmount = (EditText) findViewById(R.id.etAmount);
        setupAmountInputListener(etAmount);  // Listen on amount input

	}
	
 	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	/* When click on 10% rate button, 10% rate will be set, 
	 * if the rate is changed, try calculating tips  
	 */
	public void clickOnTenPercent(View v) {
		Button btnTenPercent = (Button)findViewById(R.id.btnTenPercent);
		Button btnFifteenPercent = (Button)findViewById(R.id.btnFifteenPercent);
		Button btnTwentyPercent = (Button)findViewById(R.id.btnTwentyPercent);
		if (tipsRate != 10) {
		    btnTenPercent.setBackgroundColor(0xff005500);		// Background set to dark green
		    btnFifteenPercent.setBackgroundColor(0xff8f8f8f);	// Restore the background of the other two buttons
		    btnTwentyPercent.setBackgroundColor(0xff8f8f8f);
		    tipsRate = 10;
		    calculateTips();
		}
		
        return;		
	}
	
	/* When click on 15% rate button, 15% rate will be set, 
	 * if the rate is changed, try calculating tips  
	 */
	public void clickOnFifteenPercent(View v) {
		Button btnTenPercent = (Button)findViewById(R.id.btnTenPercent);
		Button btnFifteenPercent = (Button)findViewById(R.id.btnFifteenPercent);
		Button btnTwentyPercent = (Button)findViewById(R.id.btnTwentyPercent);
		if (tipsRate != 15) {
		    btnFifteenPercent.setBackgroundColor(0xff005500);	// Background set to dark green
		    btnTenPercent.setBackgroundColor(0xff8f8f8f);		// Restore the background of the other two buttons
		    btnTwentyPercent.setBackgroundColor(0xff8f8f8f);
		    tipsRate = 15;
		    calculateTips();
		}
		
        return;		
	}

	/* When click on 20% rate button, 20% rate will be set, 
	 * if the rate is changed, try calculating tips  
	 */
	public void clickOnTwentyPercent(View v) {
		Button btnTenPercent = (Button)findViewById(R.id.btnTenPercent);
		Button btnFifteenPercent = (Button)findViewById(R.id.btnFifteenPercent);
		Button btnTwentyPercent = (Button)findViewById(R.id.btnTwentyPercent);
		if (tipsRate != 20) {
		    btnTwentyPercent.setBackgroundColor(0xff005500);	// Background set to dark green
		    btnTenPercent.setBackgroundColor(0xff8f8f8f);		// Restore the background of the other two button
		    btnFifteenPercent.setBackgroundColor(0xff8f8f8f);
		    tipsRate = 20;
		    calculateTips();
		}
		
        return;		
	}
	
	/* Calculate the tips based on the amount inputed and the rate selected
	 * 
	 */
	public void calculateTips() {
		EditText etAmount = (EditText)findViewById(R.id.etAmount);
		
		try {
			String amountText = etAmount.getText().toString();
			String tipsText = "";
			if (amountText.length()> 0 && tipsRate>0) {  // No tips when no amount input, or no rate selected
		        float amount = Float.parseFloat(etAmount.getText().toString());
		        float tips = amount * tipsRate / 100f;
			    DecimalFormat df = new DecimalFormat("#0.##");    // Two decimal places
			    tipsText = "$" + df.format(tips);
			}
			TextView tvTips = (TextView)findViewById(R.id.tvTips);
			tvTips.setText("Pay tips: " + tipsText);
		}
		catch (NumberFormatException nfe) {
			Toast.makeText(MainActivity.this, "Unrecoginizable amount!",Toast.LENGTH_SHORT).show();
		}
		return;
		
	}
	
	/* Amount edittext input listener
	 * 
	 */
	private void setupAmountInputListener(EditText etAmount) {
	       etAmount.addTextChangedListener(new TextWatcher() {

	           public void afterTextChanged(Editable s) {
	        	   // Any text change, just calcuate the tips
                   calculateTips();
	           }
	           public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
	           public void onTextChanged(CharSequence s, int start, int before, int count) {}	
	       });
	}

}
